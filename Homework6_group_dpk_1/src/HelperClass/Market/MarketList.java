/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Market;

import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class MarketList {

    private ArrayList<Market> marketArrayList;

    public MarketList() {
        marketArrayList = new ArrayList<>();
    }

    public ArrayList<Market> getMarketArrayList() {
        return marketArrayList;
    }

    private void setMarketArrayList(ArrayList<Market> marketArrayList) {
        this.marketArrayList = marketArrayList;
    }

    public Market addNewMarket() {
        Market market = new Market();
        marketArrayList.add(market);
        return market;
    }

}
