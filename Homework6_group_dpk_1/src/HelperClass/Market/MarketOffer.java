/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Market;

import HelperClass.Product.Product;

/**
 *
 * @author Deepak Chandwani
 */
public class MarketOffer {

    private Market market;
    private double floorPrice;
    private double targetPrice;
    private double ceilPrice;

    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(double targetPrice) {
        this.targetPrice = targetPrice;
    }

    public double getCeilPrice() {
        return ceilPrice;
    }

    public void setCeilPrice(double ceilPrice) {
        this.ceilPrice = ceilPrice;
    }

}
