/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.MasterOrder;

import java.util.ArrayList;
import HelperClass.Order.OrderDirectory;

/**
 *
 * @author Deepak Chandwani
 */
public class MasterOrderDirectory {

    ArrayList<OrderDirectory> masterOrderArrayList;

    public MasterOrderDirectory() {
        masterOrderArrayList = new ArrayList<>();
    }

    public ArrayList<OrderDirectory> getMasterOrderArrayList() {
        return masterOrderArrayList;
    }

    public void setMasterOrderArrayList(ArrayList<OrderDirectory> masterOrderArrayList) {
        this.masterOrderArrayList = masterOrderArrayList;
    }

    public OrderDirectory addOrder() {
        OrderDirectory orderDirectory = new OrderDirectory();
        masterOrderArrayList.add(orderDirectory);
        return orderDirectory;
    }

    public void addOrder(OrderDirectory orderDirectory) {
        masterOrderArrayList.add(orderDirectory);
    }

    public void removeOrder(OrderDirectory orderDirectory) {
        masterOrderArrayList.remove(orderDirectory);
    }

}
