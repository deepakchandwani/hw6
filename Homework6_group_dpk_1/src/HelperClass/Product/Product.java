/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Product;

import HelperClass.Market.MarketOffer;
import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class Product {

    int ProductId;
    String Name;
    String Description;
    int Availability;
    //MarketOffer marketOffer;
    ArrayList<MarketOffer> marketOfferArrayList;
    static int count = 0;

    public Product() {
        count++;
        setProductId(count);
        marketOfferArrayList = new ArrayList<>();
    }

    public int getProductId() {
        return ProductId;
    }

    private void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getAvailability() {
        return Availability;
    }

    public void setAvailability(int Availability) {
        this.Availability = Availability;
    }

    @Override
    public String toString() {
        return Name;
    }

    public ArrayList<MarketOffer> getMarketOfferArrayList() {
        return marketOfferArrayList;
    }

    public void setMarketOfferArrayList(ArrayList<MarketOffer> marketOfferArrayList) {
        this.marketOfferArrayList = marketOfferArrayList;
    }

    public MarketOffer addProductMarketOffer() {
        MarketOffer marketOffer = new MarketOffer();
        marketOfferArrayList.add(marketOffer);
        return marketOffer;
    }

    public void removeMarketOffer(MarketOffer mo) {
        marketOfferArrayList.remove(mo);
    }
}
