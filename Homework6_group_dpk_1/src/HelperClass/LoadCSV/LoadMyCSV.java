/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.LoadCSV;

import Business.Business;
import HelperClass.Customer.Customer;
import HelperClass.Customer.CustomerDirectory;
import HelperClass.Employee.Employee;
import HelperClass.Employee.EmployeeDirectory;
import HelperClass.EmployeeRole.EmployeeRole;
import HelperClass.Encrypt.Encrypt;
import HelperClass.Market.Market;
import HelperClass.Market.MarketList;
import HelperClass.Market.MarketOffer;
import HelperClass.Product.Product;
import HelperClass.Product.ProductDirectory;
import HelperClass.Supplier.Supplier;
import HelperClass.Supplier.SupplierDirectory;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Deepak Chandwani
 */
public class LoadMyCSV {

  //String csvPath = "C:\\Users\\bothr\\Documents\\New folder\\hw6\\Homework6_group_dpk_1\\src\\HelperClass\\CSVFiles\\";
   String csvPath = "C:/Users/Deepak Chandwani/aed/HW6/hw6/Homework6_group_dpk_1/src/HelperClass/CSVFiles/";
 //   String csvPath = "C:\\Users\\bothr\\Documents\\New folder\\hw6\\Homework6_group_dpk_1\\src\\HelperClass\\CSVFiles\\";
    // String csvPath = "C:/Users/Deepak Chandwani/Dropbox/NetBeansProjects/AED/Homework6_group/src/HelperClass/CSVFiles/";
    //String csvPath = "C:/Users/bharg/AED_LAB/New HW6/hw6/Homework6_group_dpk_1/src/HelperClass/CSVFiles/";

    String line = "";
    String cvsDataSeparator = ",";
    String fileName = null;
    String fileProducts = "Product.csv";
    String fileEmployee = "Employee.csv";
    String fileCustomer = "Customer.csv";
    String fileSupplier = "Supplier.csv";
    String fileMarket = "Market.csv";
    //String[] product;

    private Business business;

    //private Customer customer;
    private CustomerDirectory customerDirectory;

    //private Employee employee;
    private EmployeeDirectory employeeDirectory;

    //private Supplier supplier;
    private SupplierDirectory supplierDirectory;

    private MarketList marketList;

    //private Product product;
    private ProductDirectory productDirectory;

    public LoadMyCSV(Business business) {
        this.business = business;
        this.customerDirectory = business.getCustomerDirectory();
        this.employeeDirectory = business.getEmployeeDirectory();
        this.supplierDirectory = business.getSupplierDirectory();
        this.productDirectory = business.getProductDirectory();
        this.marketList = business.getMarketList();

        readEmployee();
        readCustomer();
        readMarket();
    }

    private void readEmployee() {
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileEmployee)))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] employee = line.split(cvsDataSeparator);
                    //System.out.println(line);

                    Employee e = employeeDirectory.addEmployee();
                    e.setName(employee[1]);
                    e.setUserName(employee[2]);
                    int p = new Encrypt().encrypt(employee[3]);
                    e.setPassword(p);
                    EmployeeRole er = new EmployeeRole();
                    er.setRole(employee[4]);
                    e.setEmployeeRole(er);
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
        readSupplier();
    }

    private void readMarket() {
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileMarket)))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] market = line.split(cvsDataSeparator);
                    //System.out.println(line);
                    Market mkt = marketList.addNewMarket();
                    mkt.setMarketType(market[0]);
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
    }

    private void readCustomer() {
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileCustomer)))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] customer = line.split(cvsDataSeparator);
                    //System.out.println(line);

                    Customer c = customerDirectory.addCustomer();
                    c.setName(customer[0]);
                    c.setAddress(customer[1]);
                    Market m = new Market();
                    m.setMarketType(customer[2]);
                    c.setMarketType(m);
                    c.setContactPersonName(customer[3]);
                    c.setContactPersonCell(customer[4]);
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
    }

    private void readSupplier() {
        //String supplier = null;
        ArrayList<Supplier> tempSuplier = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileSupplier)))) {
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] supplier = line.split(cvsDataSeparator);
                    //System.out.println(line);                    
                    Supplier s = new Supplier();
                    s.setName(supplier[0]);
                    tempSuplier.add(s);
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
        //tempSuplier = new RemoveDuplicates().removeDuplicates(tempSuplier);
        ArrayList<String> x = new ArrayList<>();
        for (int i = 0; i < tempSuplier.size(); i++) {
            Supplier s = tempSuplier.get(i);
            if (!(x.contains(s.getName()))) {
                supplierDirectory.addSupplier(s);
                //System.out.println("Supplier s " + s.getName());
            }
        }
        readSupplierProduct();
    }

    private void readSupplierProduct() {

        //System.out.println("Supplier size " + String.valueOf(business.getSupplierDirectory().getSupplierDirectotyArrayList().size()));
        for (Supplier s : business.getSupplierDirectory().getSupplierDirectotyArrayList()) {
            s.setProductDirectory(readProduct(s));
        }
    }

    private ProductDirectory readProduct(Supplier supplier) {
        //String supplier = null;
        ProductDirectory pD = new ProductDirectory();
        ArrayList<Product> tempProduct = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvPath.concat(fileProducts)))) {
            int i = 0;
            tempProduct.clear();
            while ((line = br.readLine()) != null) {
                if (i >= 1) {
                    String[] product = line.split(cvsDataSeparator);
                    //System.out.println("Product: "+line);

                    if (product[0].equalsIgnoreCase(supplier.getName())) {

                        Product p = business.getProductDirectory().addProduct();
                        p.setName(product[2]);
                        //System.out.println(" ");
                        //System.out.println(" ");
                        //System.out.println(" ");
                        //System.out.println("productName : "+product[2]);
                        p.setAvailability(Integer.parseInt(product[3]));
                        p.setDescription(product[4]);

                        ////// Education Market
                        MarketOffer mEdu = p.addProductMarketOffer();
                        Market mktEdu = new Market();
                        mktEdu.setMarketType(product[5]);
                        //System.out.println("product[5] : "+product[5]);
                        mEdu.setMarket(mktEdu);
                        mEdu.setProduct(p);
                        mEdu.setFloorPrice(Double.parseDouble(product[6]));
                        mEdu.setCeilPrice(Double.parseDouble(product[7]));
                        mEdu.setTargetPrice(Double.parseDouble(product[8]));

                        ////// Enterprise Market
                        MarketOffer mEnter = p.addProductMarketOffer();
                        Market mktEnter = new Market();
                        mktEnter.setMarketType(product[9]);
                        //System.out.println("product[9] : "+product[9]);
                        mEnter.setMarket(mktEnter);
                        mEnter.setProduct(p);
                        mEnter.setFloorPrice(Double.parseDouble(product[10]));
                        mEnter.setCeilPrice(Double.parseDouble(product[11]));
                        mEnter.setTargetPrice(Double.parseDouble(product[12]));

                        tempProduct.add(p);
                    }
                } else {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong in loading CSV");
        }
        //System.out.println("tempProduct size " + String.valueOf(tempProduct.size()));
        pD.setProductDirectoryArrayList(tempProduct);
        return pD;
    }
}
