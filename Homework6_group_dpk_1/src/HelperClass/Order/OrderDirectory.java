/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Order;

import HelperClass.Customer.Customer;
import HelperClass.Employee.Employee;
import java.util.ArrayList;

/**
 *
 * @author Deepak Chandwani
 */
public class OrderDirectory {

    private ArrayList<OrderItem> orderArrayList;
    private Customer customer;
    private double orderTotal;
    private double orderCommission;
    private int itemsCount;
    private Employee employee;

    public OrderDirectory() {
        orderArrayList = new ArrayList<>();
    }

    public ArrayList<OrderItem> getOrderArrayList() {
        return orderArrayList;
    }

    public void setOrderArrayList(ArrayList<OrderItem> orderArrayList) {
        this.orderArrayList = orderArrayList;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }

    public int getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public double getOrderCommission() {
        return orderCommission;
    }

    public void setOrderCommission(double orderCommission) {
        this.orderCommission = orderCommission;
    }

}
