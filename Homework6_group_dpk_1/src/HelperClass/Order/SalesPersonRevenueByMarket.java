/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HelperClass.Order;

import HelperClass.Employee.Employee;

/**
 *
 * @author bharg
 */
public class SalesPersonRevenueByMarket {
    
    private Employee emp;
    private Double orderTotal;

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public Double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(Double orderTotal) {
        this.orderTotal = orderTotal;
    }
    
}
