/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import HelperClass.Customer.CustomerDirectory;
import HelperClass.Employee.EmployeeDirectory;
import HelperClass.Market.MarketList;
import HelperClass.MasterOrder.MasterOrderDirectory;
import HelperClass.Order.OrderDirectory;
import HelperClass.Order.OrderItem;
import HelperClass.Product.ProductDirectory;
import HelperClass.Supplier.SupplierDirectory;

/**
 *
 * @author Deepak Chandwani
 */
public class Business {

    //private Customer customer;
    private CustomerDirectory customerDirectory;

    //private Employee employee;
    private EmployeeDirectory employeeDirectory;

    //private Supplier supplier;
    private SupplierDirectory supplierDirectory;

    //private Product product;
    private ProductDirectory productDirectory;

    //private OrderItem orderItem;
    //private OrderDirectory orderDirectory;
    private MasterOrderDirectory masterOrderDirectory;

    private MarketList marketList;

    public Business() {

        customerDirectory = new CustomerDirectory();
        setCustomerDirectory(customerDirectory);

        employeeDirectory = new EmployeeDirectory();
        setEmployeeDirectory(employeeDirectory);

        supplierDirectory = new SupplierDirectory();
        setSupplierDirectory(supplierDirectory);

        productDirectory = new ProductDirectory();
        setProductDirectory(productDirectory);

//        orderItem = new OrderItem();
//        setOrderItem(orderItem);
//        orderDirectory = new OrderDirectory();
//        setOrderDirectory(orderDirectory);
        masterOrderDirectory = new MasterOrderDirectory();
        setMasterOrderDirectory(masterOrderDirectory);

        marketList = new MarketList();
        setMarketList(marketList);
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public ProductDirectory getProductDirectory() {
        return productDirectory;
    }

    public void setProductDirectory(ProductDirectory productDirectory) {
        this.productDirectory = productDirectory;
    }

//    public OrderItem getOrderItem() {
//        return orderItem;
//    }
//
//    public void setOrderItem(OrderItem orderItem) {
//        this.orderItem = orderItem;
//    }
//
//    public OrderDirectory getOrderDirectory() {
//        return orderDirectory;
//    }
//
//    public void setOrderDirectory(OrderDirectory orderDirectory) {
//        this.orderDirectory = orderDirectory;
//    }
    public MasterOrderDirectory getMasterOrderDirectory() {
        return masterOrderDirectory;
    }

    public void setMasterOrderDirectory(MasterOrderDirectory masterOrderDirectory) {
        this.masterOrderDirectory = masterOrderDirectory;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }

}
